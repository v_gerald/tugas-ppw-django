from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST 

def home(request):
    return render(request, 'story6_ppw/home.html')

def cerita(request):
    return render(request, 'story6_ppw/home.html')

def kontak(request):
	return render(request, 'story6_ppw/home.html')

def kegiatan(request):
    form_mk = MataKuliahForm()

    context = {'form_mk' : form_mk, 'mylink' : True, 'number': 1}

    return render(request, 'story6_ppw/kegiatan.html', context)

def utambahdosen(request):
    form_mk = DosenForm()

    context = {'form_mk' : form_mk, 'mylink' : False, 'number': 2}

    return render(request, 'main/kegiatan.html', context)

def tabelmatkul(request):
	matakuliah = matkul.objects.all()

	context = {"matakuliah" : matakuliah}

	return render(request, 'main/tabelmatakuliah.html', context)


def deletematkul(request, pk):
	matkul.objects.filter(pk=pk).delete()
	new_data = matkul.objects.all()
	context = {"matakuliah": new_data} 
	return render(request, 'main/tabelmatakuliah.html', context)

@require_POST
def addMatkul(request):
	form_mk = MataKuliahForm(request.POST)
	if form_mk.is_valid():
		mk = matkul()
		mk = form_mk.save(commit=False)
		mk.save()
		form_mk.save_m2m()
		return redirect('/cerita')
	else:
		print('Invalid Form')

	return redirect('/cerita')

@require_POST
def tambahdosen(request):
	form_mk = DosenForm(request.POST)
	if form_mk.is_valid():
		mk = matkul()
		mk = form_mk.save(commit=False)
		mk.save()
		return redirect('/cerita')
	else:
		print('Invalid Form')
	return redirect('/cerita')
