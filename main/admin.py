from django.contrib import admin
from .models import Dosen, Day, MataKuliah

admin.site.register(Dosen)
admin.site.register(Day)
admin.site.register(MataKuliah)

# Register your models here.

