from django.urls import path

from . import views

app_name = 'story6_ppw'

urlpatterns = [
    path('', views.home, name='home'),
    path('kegiatan/', views.kegiatan, name='kegiatan'),
    path('kegiatan/add', views.addMatkul, name='add'),
    path('tabel_matkul/', views.tabelmatkul, name='tabelmatkul'),
    path('kegiatan/tambahdosen', views.utambahdosen, name='tambahdosen'),
    path('kegiatan/tambahdoseny', views.tambahdosen, name='tambahdoseny'),
    path('<int:pk>', views.deletematkul, name='Delete')
]
