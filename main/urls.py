from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('cerita/', views.cerita, name='cerita' ),
    path('kontak/', views.kontak, name='kontak'),
    path('tentang/', views.tentang, name='tentang'),
    path('proyek/', views.proyek, name='proyek'),
    path('gallery/', views.gallery, name='gallery'),
    path('kegiatan/', views.kegiatan, name='kegiatan'),
    path('kegiatan/add', views.addMatkul, name='add'),
    path('tabel_matkul/', views.tabelmatkul, name='tabelmatkul'),
    path('kegiatan/tambahdosen', views.utambahdosen, name='tambahdosen'),
    path('kegiatan/tambahdoseny', views.tambahdosen, name='tambahdoseny'),
    path('<int:pk>', views.deletematkul, name='Delete')
]
