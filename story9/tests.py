from django.test import TestCase, Client
import unittest
from django.http import HttpRequest
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .views import *
import time


class Story9(TestCase):

    def test_page_login(self):
        response = Client().get('/story9/login')
        self.assertEqual(response.status_code,200)
    
    def test_page_logout(self):
        response = Client().get('/story9/logout')
        self.assertEqual(response.status_code,302)

    def test_page_sign_up(self):
        response = Client().get('/story9/signup')
        self.assertEqual(response.status_code,200)
   
    def test_page_template(self):
        response = Client().get('/story9/login')
        self.assertTemplateUsed(response,'story9.html')

    def test_page_sign_up_template(self):
        response = Client().get('/story9/signup')
        self.assertTemplateUsed(response,'signup.html')

    def test_login_page_is_using_correct_function(self):
    	found = resolve('/story9/login')
    	self.assertEqual(found.func, logink)

    def test_logout_page_is_using_correct_function(self):
    	found = resolve('/story9/logout')
    	self.assertEqual(found.func, logoutk)

    def test_signup_page_is_using_correct_function(self):
    	found = resolve('/story9/signup')
    	self.assertEqual(found.func, signup)