from django.shortcuts import render, redirect
from .forms import loginForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout

# Create your views here.

def logink(request):
	if not request.user.is_authenticated:
		form = loginForm()
		if request.method == "POST":
			form = loginForm(request.POST)
			if form.is_valid():
				usrname = form.cleaned_data['username']
				usrpassw = form.cleaned_data['password']
				user = authenticate(request, username=usrname, password = usrpassw)
				if user is not None:
					login(request, user)
					response = {'form':form,'user':user}
					return redirect('/story9')
				else:
					response = {'form':form,'user':user}
					return redirect('/story9/login')
		else:
			response = {'form':form}
			return render(request,'story9.html',response)
	else:
		return redirect('/story9')

def redlogin(request):
	if request.user.is_authenticated:
		return render(request, 'redlogin.html')
	else:
		return redirect('/story9/login')

def logoutk(request):
    logout(request)
    form = loginForm()
    response = {'form':form}
    return redirect('/story9')

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/story9')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})