from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST 
from .models import MataKuliah as matkul
from .models import Dosen as dosenk
from .models import Day as day
from .forms import DosenForm, MataKuliahForm, DayForm

def home(request):
    return render(request, 'main/home.html')

def cerita(request):
	return render(request, 'main/ceritasatu.html')

def kontak(request):
	return render(request, 'main/kontak.html')

def tentang(request):
	return render(request, 'main/tentang.html')

def gallery(request):
	return render(request, 'main/gallery.html')

def proyek(request):
	return render(request, 'main/proyek.html')

def kegiatan(request):
    form_mk = MataKuliahForm()

    context = {'form_mk' : form_mk, 'mylink' : True, 'number': 1}

    return render(request, 'main/kegiatan.html', context)

def utambahdosen(request):
    form_mk = DosenForm()

    context = {'form_mk' : form_mk, 'mylink' : False, 'number': 2}

    return render(request, 'main/kegiatan.html', context)

def tabelmatkul(request):
	matakuliah = matkul.objects.all()

	context = {"matakuliah" : matakuliah}

	return render(request, 'main/tabelmatakuliah.html', context)


def deletematkul(request, pk):
	matkul.objects.filter(pk=pk).delete()
	new_data = matkul.objects.all()
	context = {"matakuliah": new_data} 
	return render(request, 'main/tabelmatakuliah.html', context)

@require_POST
def addMatkul(request):
	form_mk = MataKuliahForm(request.POST)
	if form_mk.is_valid():
		mk = matkul()
		mk = form_mk.save(commit=False)
		mk.save()
		form_mk.save_m2m()
		return redirect('/cerita')
	else:
		print('Invalid Form')

	return redirect('/cerita')

@require_POST
def tambahdosen(request):
	form_mk = DosenForm(request.POST)
	if form_mk.is_valid():
		mk = matkul()
		mk = form_mk.save(commit=False)
		mk.save()
		return redirect('/cerita')
	else:
		print('Invalid Form')
	return redirect('/cerita')
