from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.

def hm(request):
	response = {}
	return render(request, 'story8/home.html',response)


def data_function(request):
	arg = request.GET['q']
	target_url = 'https://www.googleapis.com/books/v1/volumes?q=' + arg;
	r = requests.get(target_url)
	data = json.loads(r.content)
	return JsonResponse(data, safe=False)

