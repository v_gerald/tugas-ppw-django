from django import forms
from django.forms import ModelForm
from .models import Dosen, Day, MataKuliah
from .admin import *


class DosenForm(forms.ModelForm):

	class Meta:
		model = Dosen
		fields = ('first_name','last_name') 
		help_texts = {'first_name':'Isi nama pertama.', 'last_name':'Isi nama terakhir.'}

class MataKuliahForm(forms.ModelForm):

	deskripsi_text = forms.CharField( widget=forms.Textarea)
	days = forms.ModelMultipleChoiceField(queryset=Day.objects, required=True)

	class Meta:
		model = MataKuliah
		fields = ['name','semester_tahun','sks','deskripsi_text','ruang_kelas','dosen_kuliah','days']

class DayForm(forms.ModelForm):

	class Meta:
		model = Day
		fields = ['name']




