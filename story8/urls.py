from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.hm, name='hm'),
    path('data/', views.data_function)
]
