from django.test import TestCase, Client
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class Story8Test (TestCase):
	def test_home8_is_exist(self):
		response = Client().get('/story8ppw/')
		self.assertEqual(response.status_code,200)

	def test_home8_is_using_home_function(self):
		found = resolve('/story8ppw/')
		self.assertEqual (found.func,hm)

	def test_home8_is_using_correct_html(self):
		response = Client().get('/story8ppw/')
		self.assertTemplateUsed(response, 'story8/home.html')

	def test_data_page_is_using_correct_function(self):
		found = resolve('/story8ppw/data/')
		self.assertEqual(found.func, data_function)

#class TestSignup(TestCase):

    #def setUp(self):
    #    self.driver = webdriver.Firefox()

  #  def test_signup_fire(self):
   #     self.driver.get("http://localhost:8000/story8ppw")
   #     self.driver.find_element_by_id('keyword').send_keys("A")
  #      self.assertTrue(driver.find_element_by_id('stb').size() != 0)
   #     self.assertIn("http://localhost:8000/story8ppw", self.driver.current_url)
  #      my_text = "A"
  #      self.assertTrue(my_text in driver.page_source)

  #  def tearDown(self):
    #    self.driver.quit