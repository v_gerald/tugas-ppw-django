
$(document).ready(function() {
	$("#keyword").keyup( function() {
		var my_keyword = $("#keyword").val();
		var called_url = '/story8ppw/data?q=' + my_keyword;

		$.ajax({
			url: called_url, success: function(stb) {
				console.log(stb.items)
				var received_object = $("#stb");
				received_object.empty();
				received_object.append(	'<tr><th>No.</th><th>Title</th><th>Thumbnail</th><th>Author</th><th>Publisher</th><th>Published Date</th></tr>');

				for (i = 0; i < stb.items.length; i++) {
					var k;
					var tmp_title = stb.items[i].volumeInfo.title;
					var tmp_thumbnail = stb.items[i].volumeInfo.imageLinks.smallThumbnail
					if (tmp_thumbnail === undefined) {
						tmp_thumbnail = '';
					} else {
						k = '<img src="' + tmp_thumbnail + '" >'
					}

					var tmp_author;
					if (stb.items[i].volumeInfo.authors === undefined) {
						tmp_author = '';
					} else {
						tmp_author = stb.items[i].volumeInfo.authors[0];
					}
					var tmp_publisher = stb.items[i].volumeInfo.publisher;
					if (tmp_publisher === undefined) {
						tmp_publisher = '-';
					}

					var tmp_publishedDate = stb.items[i].volumeInfo.publishedDate;
					if (tmp_publishedDate === undefined) {
						tmp_publishedDate = '';
					}

					console.log(tmp_title);
					received_object.append( '<tr><td>'+ (i+1).toString() + '</td><td>' + tmp_title + '</td>' + '<td>' + k +' </td><td>' + tmp_author + '  </td><td>' + tmp_publisher + '  </td><td> '+ tmp_publishedDate +'</td></tr>');
				}
			}
		})
	})
});
