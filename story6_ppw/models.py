from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
class Dosen(models.Model):
	first_name = models.CharField(max_length=40)
	last_name = models.CharField(max_length=40,blank=True)

	def __str__(self):
		return "{0} {1}".format(self.first_name, self.last_name)

class Day(models.Model):
	name = models.CharField(max_length=30)

	def __str__(self):
		return self.name

class MataKuliah(models.Model):
	GS_2020 = 'Gasal 2020/2021'
	GN_2019 = 'Genap 2019/2020'
	GS_2019 = 'Gasal 2018/2019'
	GN_2018 = 'Genap 2018/2019'
	GS_2018 = 'Gasal 2018/2019'
	GN_2017 = 'Genap 2017/2018'

	SEMESTER_TAHUN_CHOICES = ((GS_2020, 'Gasal 2020/2021'), (GN_2019, 'Genap 2019/2020'), (GS_2019, 'Gasal 2019/2020'), (GN_2018, 'Genap 2018/2019'), (GS_2018, 'Gasal 2018/2019') ,(GN_2017, 'Genap 2017/2018'))

	semester_tahun = models.CharField(max_length=40, choices=SEMESTER_TAHUN_CHOICES, blank=False, default=GS_2020)
	name = models.CharField(max_length=50)
	sks = models.PositiveIntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(20)])
	deskripsi_text = models.CharField(max_length=400)
	ruang_kelas = models.CharField(max_length=100)
	dosen_kuliah = models.ManyToManyField(Dosen)
	days = models.ManyToManyField(Day)

	def __str__(self):
		return "{0} {1}".format(self.name, self.semester_tahun)