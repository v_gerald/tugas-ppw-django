from django.urls import path

from . import views

app_name = 'story9'

urlpatterns = [
    path('login', views.logink, name='logink'),
    path('logout', views.logoutk, name='logoutk'),
    path('signup', views.signup, name='signupk'),
    path('', views.redlogin, name='redlogin'),
]
